package com.dinhlap.mytnh

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var soA : Int = 0
    private var ketQua : Int = 0
    private var phepTinh : String = ""
    private val cong : String = "+"
    private val tru : String = "-"
    private val nhan : String = "x"
    private val chia : String = ":"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonKetQua.visibility = View.INVISIBLE
        iVclosed.setOnClickListener(){
          finish()
        }
    }

    fun onClick(view: View) {
        when (view.id){
            buttonCong.id ->{
                if(!edtNumber.text.toString().equals("")){
                    setHide()
                    soA = edtNumber.text.toString().toInt()
                    edtNumber.setText("")
                    phepTinh = cong
                    tvKetQua.setText(soA.toString()+ " +")
                }
            }

            buttonTru.id ->{
                if(!edtNumber.text.toString().equals("")){
                    setHide()
                    soA = edtNumber.text.toString().toInt()
                    edtNumber.setText("")
                    phepTinh = tru
                    tvKetQua.setText(soA.toString()+ " -")
                }
            }

            buttonNhan.id ->{
                if(!edtNumber.text.toString().equals("")){
                    setHide()
                    soA = edtNumber.text.toString().toInt()
                    edtNumber.setText("")
                    phepTinh = nhan
                    tvKetQua.setText(soA.toString()+ " X")
                }
            }

            buttonChia.id ->{
                if(!edtNumber.text.toString().equals("")){
                    setHide()
                    soA = edtNumber.text.toString().toInt()
                    edtNumber.setText("")
                    phepTinh = chia
                    tvKetQua.setText(soA.toString()+ " :")
                }
            }
            buttonKetQua.id ->{
                when(phepTinh){
                    cong -> {
                        if(!edtNumber.text.toString().equals("")){
                            ketQua = soA + edtNumber.text.toString().toInt()
                            tvKetQua.setText("Kết quả :" + ketQua.toString())
                            edtNumber.setText("")
                            setVisible()
                        }else{
                            setHide()
                        }
                    }
                    tru -> {
                        if(!edtNumber.text.toString().equals("")){
                            ketQua = soA - edtNumber.text.toString().toInt()
                            tvKetQua.setText("Kết quả :" + ketQua.toString())
                            edtNumber.setText("")
                            setVisible()
                        }else{
                            setHide()

                        }
                    }
                    nhan -> {
                        if(!edtNumber.text.toString().equals("")){
                            ketQua = soA * edtNumber.text.toString().toInt()
                            tvKetQua.setText("Kết quả :" + ketQua.toString())
                            edtNumber.setText("")
                            setVisible()
                        }else{
                            setHide()
                        }
                    }
                    chia -> {
                        if(!edtNumber.text.toString().equals("")){
                            ketQua = soA / edtNumber.text.toString().toInt()
                            tvKetQua.setText("Kết quả :" + ketQua.toString())
                            edtNumber.setText("")
                            setVisible()
                        }else{
                            setHide()
                        }
                    }
                }
            }
        }
    }
    private fun setVisible(){
        buttonCong.visibility = View.VISIBLE
        buttonTru.visibility = View.VISIBLE
        buttonNhan.visibility = View.VISIBLE
        buttonChia.visibility = View.VISIBLE
        buttonKetQua.visibility = View.INVISIBLE
    }
    private fun setHide(){
        buttonCong.visibility = View.INVISIBLE
        buttonTru.visibility = View.INVISIBLE
        buttonNhan.visibility = View.INVISIBLE
        buttonChia.visibility = View.INVISIBLE
        buttonKetQua.visibility = View.VISIBLE
    }
}